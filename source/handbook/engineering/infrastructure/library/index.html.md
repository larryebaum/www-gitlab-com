---
layout: markdown_page
title: "Library"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

The **Library** contains the *current state* of Infrastructure's thinking about the problems we are solving. Thei documentation contained in the Library can be in one of three states: a **blueprint**, a **design**, or the relevant **architectural** documentation.

### Blueprints

**Blueprints** are intended to flesh out our thinking about specific ideas we need to implement and/or problems we need to solve. Blueprints are sketches whose purpose is to foster and frame discussion around Infrastructure topics. They  are one of the inputs to [DNA meetings](../meeting/#design-and-automation-dna), and will generally yield one of more designs.

* Blueprints are required for, but not limited to, projects and KRs. All projects and KRs require a blueprint, as they define, scope, and outline considerations, options and recommendations. They should result in a clear understanding of the topic and widespread agreement on the specific deliverables that implement a project or KR. 

* Blueprints can also be associated with items other than projects and KRs: whenever we are considering whether we should tackle a complex problem and how we might approach it, a blueprint is recommended. 

As a guideline, a blueprint are structured as follows:

* Idea or problem statement: provide background and scope the idea or problem discussed in the blueprint
* Summary of past efforts (if applicable)
* Outline of options
* Additional considerations
  * Dependencies
  * Related blueprints or designs
  * Costs
* Recommendations

##### Workflow

* Open issue to track blueprint work and submit a Handbook MR.
* Keep design documents in `/handbook/engineering/infrastructure/library/`
* Keep a link to the appropriate issue in the page
* Make sure you get the correct team input you need.

### Design

As GitLab.com's levels of functionality and scale increase, so does its complexity. A disciplined approach to meet
the challenges presented by said complexity is necessary so we can frame technical discussions and increase the
leverage on technical decisions: **design**.

The late Andy Grove's [*High Output Management*](https://openlibrary.org/books/OL533591M/High_output_management)
highlights his thinking on business reports as an information medium, and these thoughts apply to design as well:

> As [design] is formulated and written, authors are forced to be more precise than they might verbally. Hence, their
value stems from the discipline and the thinking the writers are forced to impose upon themselves and they identify
and deal with trouble spots in their presentation. [Designs] are more a *medium* of *self-discipline* than a way
to communicate information. *Writing* the design is important; reading may not be so as much.

Reading is, of course, important, as it drives information and knowledge sharing as well as healthy technical discussions.
This makes the writing vital. Design clarifies and scopes the problem, drives productive discussions, and presents
decisions concisely.

Also from the same book:

> All production flows have a basic characteristic: the material becomes more valuable as it moves through the
process. [...] A common rule we should always try to heed is to detect and fix any problems in a production process
at the *lowest-value* stage possible.

Thus, early scoping and design is a valuable investment.

Design is not intended to force all decisions up front but to methodically drive them. Time invested in design eliminates
confusion and ambiguity, particularly as it relates to scope, and ensures specific aspects of the engineering work we
perform take place (monitoring being one obvious case).

##### Workflow

* Open issue to track design work and submit a Handbook MR with the design.
* Keep design documents in `/handbook/engineering/infrastructure/library/`
* Keep a link to the appropriate issue in the design page
* Make sure you get the correct team input you need.

Security must be looped in whenever changes are made to any of the following areas:

1. Processing credentials/tokens
1. Storing credentials/tokens
1. Logic for privilege escalation
1. Authorization logic
1. User/account access controls
1. Authentication mechanisms
1. Abuse-related activities

# Directory

* [**Canary**](canary/) [`infra/5025`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5025)
* [**Chef Automation**](chef-automation/) [`infra/5078`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5078)
* [**CICD Omnibus**](cicd-pipeline/) [`release/framework/39`](https://gitlab.com/gitlab-org/release/framework/issues/39)
* [**Deployer**](deployer/) [`release/framework/40`](https://gitlab.com/gitlab-org/release/framework/issues/39)
* [**Disaster Recovery**](disaster-recovery/) [`infra/4741`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4741)
* [**Infrastructure Git Workflow**](git-workflow/) [`infra/5276`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4741)
* [**GitLab OKRs**](gitlab-okrs/) [`infra/6025`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6025)
* [**Kubernetes Clusters Designations**](kubernetes-clusters-designations/) [`infra/6681`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6681)
* [**Kubernetes Configuration**](kubernetes-configuration) [`gitlab-com/&64`](https://gitlab.com/groups/gitlab-com/-/epics/64)
* [**Kubernetes Traffic Transition**](kubernetes-transition-frontend-traffic/) [`infra/6673`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6673)
* [**Merging CE and EE Codebases**](merge-ce-ee-codebases/) [`release/framework/nn`](https://gitlab.com/gitlab-org/release/framework/)
* [**Terraform Automation**](terraform-automation/) [`infra/5079`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5079)
* [**PostgreSQL Database Bloat**](postgres-bloat/)[`infra/5924`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5924)
* [**Scheduled Daily Deployments**](scheduled-daily-deployments/) [`gitlab-org/release/epics/13`](https://gitlab.com/groups/gitlab-org/release/-/epics/13)
* [**Security Fixes Development Location**](security-releases-development/) [`gitlab-org/gitlab-ce/issues/55648`](https://gitlab.com/gitlab-org/gitlab-ce/issues/55648)
* [**Service Inventory Catalog**](service-inventory-catalog/) [`infra/5926`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5926)
* [**Snowplow**](snowplow/) [`infra\4348`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4348)
* [**Vault**](vault/) [`infra/epics/62`](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/62)
* [**ZFS Filesystem**](zfs-filesystem/)
* [**ZFS For Repository Storage nodes**](zfs-repo-storage/) [`gitlab-com/gl-infra/epics/65`](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/65)
* [**Deltas**](production/deltas/)
* [**Dogfooding CI/CD**](ci-cd/)
* [**OKRs**](okrs/)
* [**Planning Workflow**](planning/)
* [**Security Releases**](release/security/)
* [**PostgreSQL bloat**](database/postgres/bloat/)
* [**Service Levels and Error Budgets**](service-levels-error-budgets/)
* [**Repository Storage**](storage/block/repositories/)
* [**ZFS for Repository Storage Nodes**](storage/block/repositories/zfs-repo-storage/)
