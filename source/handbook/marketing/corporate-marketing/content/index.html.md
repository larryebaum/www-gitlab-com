---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Content team includes audience development, editorial, social marketing, video production, and content strategy, development, and operations. The Content Marketing team is responsible for the stewardship of GitLab's audiences, users, customers, and partners' content needs. Content marketing creates engaging, inspiring, and relevant content, executing integrated content programs to deliver useful and cohesive content experiences that build trust and preference for GitLab.

Roles on the Content Marketing team include:

- [Editorial](/job-families/marketing/editor/)
- [Content Marketing](/job-families/marketing/content-marketing/)
- [Digital Production](/job-families/marketing/digital-production-manager/)
- [Social Marketing](/job-families/marketing/social-marketing-manager/)

## Quick links

- [Blog calendar](/handbook/marketing/blog/#blog-calendar)
- [Content marketing schedule](/handbook/marketing/corporate-marketing/content/schedule/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Editorial team page (including blog style guide)](/handbook/marketing/corporate-marketing/content/editorial-team)
- [GitLab blog](/handbook/marketing/blog)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter)
- [Social marketing handbook](/handbook/marketing/corporate-marketing/social-marketing/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Mission & vision

Our content marketing mission statement mirrors our [company mission](/company/strategy/#mission). We strive to foster an open, transparent, and collaborative world where all digital creators can be active participants regardless of location, skillset, or status. This is the place we share our community's success and learning, helpful information and advice, and inspirational insights.

Our vision is to build the largest and most diverse community of cutting edge co-conspirators who are defining and creating the next generation of software development practices. Our plan to turn our corporate blog in to a digital magazine will allow us to add breadth, depth and support to our participation in and coverage of this space.

## Content team responsibilities

- Content pillar production
- Customer content creation
- Video production
- Blog management including writing, editing, and scheduling
- Branded YouTube management
- Organic branded social media mangement (Twitter, Facebook, and LinkedIn)
- Social media campaigns

## Communication

**Chat**

Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#blog` RSS feed
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

**Issue trackers**
 - [Blog](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)
 - [Content by stage](https://gitlab.com/groups/gitlab-com/-/boards/1136104)
 - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
 - [Digital production](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/1120979?&label_name[]=Video%20project)
 - [Social marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/968633?&label_name[]=Social)

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist and it's a:
   1. Blog post: See the [blog handbook](/handbook/marketing/blog)
   1. Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `content marketing`
   1. Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days' notice.

## Content production

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter and published each piece of content as it's completed.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events
1. Newsworthiness
1. Brand awareness opportunity

We align our content production to pillars and topics to ensure we're creating a cohesive set of content for our audience to consume. Pillar content is multi-purpose and can be plugged into integrated campaigns, event campaigns, and sales plays.

**Defintions**

- Theme: A high-level GTM message that doesn't change often. Themes are tracked as `Parent Epics`.  
- Pillar: A story within a theme. Pillars are tracked as `Child Epics`.
- Set: A topical grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`.
- Resource: An informative asset, such as an eBook or report, that is often gated.

**Content stage defintions** 

- Awareness (Top): Buyer is aware of a need or desire. Content at this stage should establish brand familiarity and trust by unbiasly helping the consumer learn about their problem, desire, or need.
- Consideration (Middle): Buyer is considering options and alternatives to acheive their need or desire. Content at this stage should help the buyer understand what GitLab is, it's capabilities, and how it compares to alternatives.
- Purchase (Bottom): Buyer is ready to purchase a product. Content at this stage should enable the consumer to easily make a purchase. For our buyers, this might include product documentation.

### Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

### Tracking content 



| Topic | Use | Example |
| ------ | ------ | ------ | 
| AWS | Content that relates Amazon Web Services. Likely use cases are case studies where the customer uses GitLab + AWS and integration information & tutorials. | [How to set up multi-account AWS SAM deployments with GitLab CI/CD](/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/) |
| Agile delivery | Content that relates to the agile delivery process decision framework which emphasizes incremental and iterative planning. | [What is an Agile mindset?](/2019/06/13/agile-mindset/) |
| Agile software development | Content that relates to the agile software development methodology which emphasizes cross-functional collaboration, continual improvement, and early delivery | [How to use GitLab for Agile software development](/2018/03/05/gitlab-for-agile-software-development/) |
| Application modernization | Content that relates to the process of converting, refactoring, re-writing, or porting legacy systems to more modern programming and infrastructure. Content on this topic may cover cost/benefit of updating legacy systems, process, system, and culture changes, and toolstack comparisons. | [3 Strategies for implementing a microservices architecture](/2019/06/17/strategies-microservices-architecture/) |
| Automation | Content that relates to using technology to automate tasks. Likely use cases are how automation impacts productivity and workflows, feature highlights & tutorials, and case studies. | [How IT automation impacts developer productivity](/2019/05/30/it-automation-developer-productivity/) |
| Azure | Content that talks specifically about Microsoft Azure. Likely uses cases are tutorials on using GitLab + Azure cloud or competitive content. | [Competitive analysis page for Azure DevOps](/devops-tools/azure-devops-vs-gitlab.html)
| CI/CD | Content that covers continuous integration, continuous delivery, and continuous deployment. This content is likely to more technical, explaining tools, methods for implementation, tutorials, and technical use cases. | [A quick guide to CI/CD pipelines](/2019/07/12/guide-to-ci-cd-pipelines/) |
| Cloud computing | Content that relates to the practice of using a network of remote servers hosted on the Internet to store, manage, and process data. Likely uses cases are content discussing various cloud models (public, private, hybrid, and multicloud), integrations, and tutorials. Some customer case studies may be tagged with this label if the case study is *primarily* about GitLab enabling their cloud computing model. | [Top 5 cloud trends of 2018: What has happened and what’s next](/2018/08/02/top-five-cloud-trends/index.html) |
| Cloud native | Content that relates container-based environments. Specifically, technologies are used to develop applications built with services packaged in containers, deployed as microservices and managed on elastic infrastructure through agile DevOps processes and continuous delivery workflows. | [A Cloud Native Transformation](/webcast/cloud-native-transformation/) |
| Containers | Content that relates to using, running, maintaining, and building for containers. | [Running Containerized Applications on Modern Serverless Platforms](https://www.youtube.com/watch?v=S8R7sSePAXQ)
| DevOps | Content that relates to DevOps methods, process, culture, and tooling. [Keys to DevOps success with Gene Kim](https://www.youtube.com/watch?v=dbkj0qXQ22A)
| DevSecOps| Content that relates specifically to integrating and automating security into the software development lifecycle. Content that relates to cybersecurity should be tagged `security` and not `devsecops`.| [A seismic shift in application security](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf) |
| Digital transformation |  | 
| GKE | Content that is specifically about Google Kubernetes engine and Google Cloud Platform. Likely use cases are integrations, tutorials, and case studies | [Demo: Deploy to GKE from GitLab](https://www.youtube.com/watch?v=u3jFf3tTtMk)
| Git | Content that relates to implementing and using the distributed version contronl system, Git. | [Moving to Git](/resources/downloads/gitlab-moving-to-git-whitepaper.pdf) |
| Jenkins | Content that is specifically about Jenkins. Likely uses cases are integrations, competitive, comparisons, and case studies. | [3 Teams left Jenkins: Here's why](2019/07/23/three-teams-left-jenkins-heres-why/) |
| Kubernetes| Contnet that relates to implementing and using kubernetes. Likely use cases are cost/benefits, tutorials, and use cases. | [Kubernetes and the future of cloud native: We chat with Kelsey Hightower](/2019/05/13/kubernetes-chat-with-kelsey-hightower/) |
| Microservices |  |
| Multicloud |  |
| Open source |  |
| SCM |  |
| Security | Content that relates to cybersecurity and application security practices. | [When technology outpaces security compliance](/2019/06/10/when-technology-outpaces-security-compliance/)
| Single application |  |
| Software development |  |
| Toolchain | Content that relates to toolchain and stack management. | [How to manage your toolchain before it manages you](/resources/downloads/201906-gitlab-forrester-toolchain.pdf)
| VSM | Content that relates to the topic of value stream mapping and management. Topics that fall under this tag may include cycle time, cycle analytics, and software delivery strategies and metrics. | [The Forrester Value Stream Management Report](/analysts/forrester-vsm/index.html) |
| Workflow | Content that relates to understanding and implementing workflows throughout the software development lifecycle. Likely uses are content that explains a particular workflow or how to set up a workflow in GitLab. For example: how a workflow might change when a level of automation is introduced. | [Planning for success](/resources/downloads/gitlab-planning-for-success-whitepaper.pdf) |

Published content should be shared to the #content-updates channel and added to PathFactory with the appropriate tags.

## Content library

Content for us is stored in PathFactory. To request access to PathFactory, submit an access request form. 

The content library in PathFactory can be filtered by content type, funnel stage, and topic. Topics are listed and defined in the section above.

## What is a content pillar?

A content pillar is a go to market content strategy that is aligned to a high-level theme (for example, Just Commit) and executed via sets. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals.  

We use content pillars to plan our work so we can provide great digital experiences to our audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, and sets help us break our work into more manageable compontents. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)

#### What's included in a content set?

Here's an example of what's included in a content set:

| Quantity | Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| 4 | Awareness | Thought leadership blog post | Content marketing |
| 1 | Awareness | Topic webpage | Content marketing |
| 1 | Awareness | Resource | Content marketing |
| 4 | Consideration| Technical blog post | Content marketing |
| 1 | Consideration | Whitepaper | Product & technical marketing |
| 1 | Consieration | Solution page | Content & product marketing |
| 2 | Consideration | Webcast | Product & technical marketing |
| 1 | Purchase | Demo | Technical marketing |
| 1 | Purchase | Data sheet | Product marketing |


**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)
- [Content for Each Buying Stage of the Consumer Purchase Cycle](https://contentwriters.com/blog/content-consumer-purchase-cycle/)

#### Current pillars

##### [Application modernization](https://gitlab.com/groups/gitlab-com/marketing/-/epics/49)

| header | header | header |
| ------ | ------ | ------ |
| 1. Organizations want to deploy faster and generate revenue through innovation but don’t have the architecture to support those goals. | 2. Legacy systems slow down development with complicated processes, open networks to security risks, and are difficult to maintain - leaving little room in IT budgets for innovating. | 3. Leveraging microservices and committing to a modernization strategy puts organizations on the path to development efficiency. |
| 4. Adopting Continuous Integration helps developers work faster and build better applications through automated testing. Errors are caught automatically so developers can focus on other tasks. | 5. Continuous Delivery/Deployment takes Continuous Integration a step further. New code is pushed more frequently through an automated release process, and developers can watch their work go live in minutes. | 6. Streamlining the toolchain and adopting cloud native architecture improves IT spend and increases efficiency by automating processes. Teams can use GitLab as the primary mechanism to configure, build, test, deploy, and manage their microservices. |

##### [Reduce cycle time](https://gitlab.com/groups/gitlab-com/marketing/-/epics/47)

Improve software cycle time to deliver business value faster.

| ------ | ------ | ------ |
| 1. Every company is becoming a software company, increasing the demand for better software, faster. Reducing the time between thinking of an idea and having the code in production is vital to providing value to customers. | 2. If an organization is too slow to innovate, its competitors can rapidly absorb the market by becoming the first organization to meet the needs of customers.  | 3. Agile planning and DevOps delivery have demonstrated reliable and scalable solutions to streamline and accelerate application delivery. |
| 4. Teams use a variety of tools to automate integration, test code, manage digital assets, scan for vulnerabilities, and deploy/configure applications, but each tool adds a layer of complexity in a team’s workflow, so teams are forced to waste cycle time tinkering on tools and not delivering business value. | 5. To reduce cycle time, teams should look for ways to consolidate interrelated development activities. Development teams need a simple, comprehensive toolchain that can easily build, test, and deliver applications without the waste and overhead of managing dozens of disparate tools and integrations. | 6. Simplifying the toolchain empowers teams to focus on innovation and delivering value to customers faster. Organizations can use GitLab to address the challenges of rapidly building and delivering business applications to remain competitive. |

Topics: Agile delivery, value stream management

##### [Secure apps](https://gitlab.com/groups/gitlab-com/marketing/-/epics/48)

Application security for developers.

| ------ | ------ | ------ |
| 1. Applications have seen exponential growth in the frequency of attacks (and successful breaches) in recent years. The rise of cloud computing, containers, and the use of third party code, in addition to the need to ship quickly and frequently, have thrust security to the forefront of software development.Security can no longer be treated as an afterthought. | 2. Regulations like GDPR and massive attacks like WannaCry and NotPetya have made security a business ultimatum. This takes the code written to protect your business to the front line of commercial cyber defense. Secure apps, APIs, and data storage could mean the difference between a 20 year future or a three year future for your company. | 3. VPs of AppDev need to work with both their direct teams and broader organizations to establish a culture of security: Everyone should feel responsible for and empowered to make your product or service as secure as possible. Security needs to be top of mind for every project and iteration. |
| 4. To help developers achieve and abide by this mindset, it’s important that they are given the education and resources necessary to understand how and why it’s critical for them to consider security from the moment they write their first line of code. | 5. Shifting left is the answer: Building security in at the end is an inefficient and cost-inducing process. If developers start with security at the beginning of the SDLC, teams will find it easier to keep security tightly coupled with updates and iterations as they work within an agile or DevOps process/methodology. | 6. Usability must be top of mind when building security in from the start. Enabling developers with a tool like GitLab - which integrates vulnerability testing into the development process, keeping developers in the same tool and reducing workflow disruption - will be crucial in getting developer buy-in.  |

Topics: application security, devsecops

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.
