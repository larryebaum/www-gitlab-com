---
layout: job_family_page
title: "Professional Services Engineer"
---

Professional Services Engineers provide professional services on-site or remote deployment of GitLab technology and solutions as well as training. The Professional Services Engineer will act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices. This role helps the customer to faster benefit from the GitLab solution and realize business value. Meanwhile, Professional Services Engineers also need to provide feedback to product management and engineering team on the actual field experience through customer engagements and implementations.

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Level Junior

Junior Professional Services Engineers share the same requirements and responsibilities outlined on our jobs page but typically join with less relevant experience, alternate background and/or less industry exposure than a typical Professional Services Engineer.`

### Responsibilities

- Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions.
- Install & configure GitLab solutions in customer environment as per Statement of Work (SOW)
- Provide technical training sessions remotely and/or on-site
- Develop and implement migration plan for customer VCS & data migration
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Develop & maintain custom scripts for integration or policy to align with custom requirements
- Create detailed documentation for implementation, guides and training.
- Support Sales on quoting PS and drafting SOW to respond PS requests
- Travel required - 40%
- Managing creation of new and maintaining of existing training content

## Level Intermediate

The Intermediate Professional Services Engineer role extends the Junior Professional Services Engineer role.

### Responsibilities

- Experienced with and proven knowledge with major cloud technologies and public cloud providers
- Experience with multiple highly available architectures and concepts. Kubernetes operational and deployment experience.
- Solve technical problems of high scope and complexity.
- Help to define and improve our internal standards for style, maintainability, and best practices for customer private/public cloud environments.

## Level Senior

The Senior Professional Services Engineer role extends the Intermediate Professional Services Engineer role.

### Responsibilities

- Mastery with and proven knowledge with major cloud technologies and public cloud providers
- Mastery with multiple highly available architectures and concepts. Kubernetes operational and deployment experience.
- Exert influence on the overall objectives and long-range goals of your team.
- Provide mentorship for Junior and Intermediate Professional Services Engineers to help them grow in their technical responsibilities and remove blockers to their autonomy.

### Requirements

- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, ChatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
  - Installation and operation of Linux operating systems and hardware investigation/manipulation commands
  - BASH/Shell scripting including systems and init.d startup scripts
  - Package management (RPM, etc. to add/remove/update packages)
  - Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)
- Experience with Ruby on Rails applications and Git

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab.

### Federal

#### Additional Requirements

- TS/SCI Security Clearance
- Knowledge of and at least 4 years of experience with Federal customers

### Embedded on-site engineer

#### Additional Responsibilities

- Embed on site with a customer for a period of 3, 6, or 12 months at a time
- Act as a GitLab evangelist onsite with customer's teams
- Identify pain points and issues with planning, development, test, compliance, security, build and orchestration processes. 
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Help review processes and identify areas for improvement including the use of automation

#### Additional Requirements

- Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
- Strong verbal and written communication skills
- High level of comfort communicating effectively across customer teams, groups or divisions
- Ideal candidates will preferably have 7 plus years IT industry or IT technical sales related experience
- Experience with modern development practices strongly preferred (Kubernetes, Docker, Linux, DevOps Pipelines (CI/CD), Application Security, Cloud providers)
- Experience with several of the following tools strongly preferred:
  - Ruby on Rails, Java, .Net, Python
  - Git, Bitbucket, GitHub, SVN
  - Jira, VersionOne, TFS
  - Jenkins, CircleCI
  - Veracode, Checkmarx, Black Duck
  - Artifactory, Nexus
  - New Relic, Nagios
  - mono-repo and distributed-repo approaches
  - BASH / Shell scripting

## Manager, Professional Services

GitLab is a hyper growth company searching for people who are intelligent, aggressive, and agile with strong skills in technology, sales, business, communication, and leadership. Desire to lead through change is a must.

The Manager of Professional Services role is a management position on the front-lines. This is a player/coach role where the individual is expected to be experienced in and have advanced insight to the GitLab platform. The individual will contribute to territory and account strategy as well as driving the execution directly and indirectly. The individual will need to be very comfortable giving and receiving positive and constructive feedback, as well as adapting to environmental change and retrospecting on successes and failures. The Professional Services Manager will work together with the other managers within the Customer Success organization to help execute on strategies and vision with the Director.

The Manager of Professional Services is a critical part of the Customer Success organization at GitLab. Made up of Professional Services Engineers, the professional services team owns the customer journey from the point of sale onward, ensuring a customer realizes the full value of GitLab throughout their organization.

You will have the opportunity to help shape and execute a strategy to help the Services team build mindshare and broad use of the GitLab platform within enterprise customers. Coaching your team members to becoming the trusted advisors to their customers. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting and management. You should also have a demonstrated ability to think strategically about business, products, and technical challenges. You will also be responsible in helping grow and maintain our enterprise-level customers.

### Responsibilities

- Work with the Customer Success Director to help establish and manage goals and responsibilities for Professional Services Engineers
- Assist in development of thought leadership, event-specific, and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts helping sell on the value of what GitLab professional services has to offer
- Ensure the Professional Services Engineers exceeds corporate expectations in core knowledge, communication, and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Monitor performance of team members and provide timely feedback and development assistance
- Create, review, and approve formal statements of work, change requests, and proposals
- Prepare weekly revenue forecast worksheet and create action plans to address issues
- Manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement improvements to the processes and tools used as a seasoned with experience leading teams of project managers and consultants in support of external customers
- Work together with our sales organization to propose, scope, and price professional services Statements of Work, including managing a PS sales overlay team
- Bring Professional Services team together with Solutions Architects and Technical Account Managers to plan and execute internal projects, ensure that teams have appropriate training and manage resources to deliver GitLab service offerings
- Provide leadership and guidance to coach, motivate and lead services team members to their optimum performance levels and career development
- Ensure delivery model is focused on quality, cost effective delivery of services, and customer success outcomes
- Remains knowledgeable and up-to-date on GitLab releases
- Documents services provided to customers, including new code, techniques, and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
- Works with the Product Engineering and Support teams, to contribute documentation for GitLab
- Help build programs that the TAMs and PSEs will execute to effectively grow our enterprise customers

### Requirements

All requirements for the Professional Services Engineer role apply to the Manager, Professional Services role, as the management role will need to understand and participate with the day-to-day aspects of the Professional Services Engineer role in addition to managing the team. Additional requirements include:

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and received positive and constructive feedback
- Able to adapt to environmental change and retrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies
- Proven track record in software/technology sales or consulting and management
- Demonstrated ability to think strategically about business, products, and technical challenges

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

- Qualified candidates for the Professional Services Engineer will receive a short questionnaire
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success
- For the Professional Services Engineer role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab
- For the Federal Professional Services Engineer role, candidates will be invited to schedule interviews with members of the Customer Success team and our Federal Regional Sales Director
- For a manager role, candidates will be invited to schedule interviews with our Regional Sales Directors for North America
- Candidates may be invited to schedule an interview with our CRO
- Finally, candidates may be asked to interview with the CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
